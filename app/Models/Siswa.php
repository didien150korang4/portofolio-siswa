<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;
    protected $fillable = ['file', 'kategori_id', 'deskripsi', 'kelas_id'];
    public function kategori()
    {
        return $this->belongsTo(Kategori::class);
    }
    public function kelas()
    {
        return $this->belongsTo(Kelas::class);
    }

}
