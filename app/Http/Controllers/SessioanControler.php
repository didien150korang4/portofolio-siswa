<?php

namespace App\Http\Controllers;

Use App\Models\User;
Use Illuminate\Support\Facades\Auth;
Use illuminate\Support\Facades\Flash;
Use illuminate\support\Facades\Session;
Use App\Http\Controllers\Controller;
Use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SessioanControler extends Controller
{
    public function index()
    {
        return view('sesi.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'userpasswordname' => 'required'
        ], [
            'username.required' => 'Username wajib diisi',
            'userpasswordname.required' => 'Password wajib diisi',
        ]
    );

    $password = $request->userpasswordname;
    $username = $request->username;

    $infologin =[
        'username' => $username,
        'password' => $password,
    ];
    // dd($infologin);
    // return response()->json($infologin);

    $find = User::where('username', $username)->first();
    if ($find) {
        if ($password == $find->password) {
            Auth::login($find);
            return redirect()->route('dashboard');
        }else{
            return redirect('')->withErrors( 'Password Salah.')->withInput();
        }
    } else {
        return redirect('')->withErrors( 'Username tidak ditemukan.')->withInput();
    }

            // if (Auth::attempt($infologin)) {
            //     // Jika autentikasi berhasil, redirect ke home
            //     return redirect('/dashboard');
            // }else{
            // // Jika autentikasi gagal, kembali ke halaman login dengan pesan kesalahan
            // return redirect('')->withErrors( 'Username atau password salah.')->withInput();
            // }
    }

    public function logout()
    {
        // Auth::logout();
        // return redirect('/login');
        Auth::logout();
        return redirect()->route('login');
    }

}
