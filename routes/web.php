<?php

use App\Http\Controllers\SessioanControler;
use App\Http\Controllers\SiswaController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/cetak', function () {
    return view('pdf.cetak');
})->name('cetak')->middleware('auth');

Route::get('/', function () {
    return view('sesi.login');
})->name('login');

Route::get('admin/dashboard', function () {
    return view('admin.dashboard');
})->name('home');

Route::get('portofolio', [SiswaController::class, 'create'])->name('portofolio')->middleware('auth');
Route::get('dashboard', [SiswaController::class, 'index'])->name('dashboard');
Route::post('/tambahkarya', [SiswaController::class, 'store'])->name('tambahkarya')->middleware('auth');
Route::get('/hapuskarya/{id}', [SiswaController::class, 'destroy'])->name('hapuskarya')->middleware('auth');
Route::get('/siswa/kategori/{id}', [SiswaController::class, 'kategori'])->name('kategory')->middleware('auth');

// Route::get('/', [SessioanControler::class, 'index'])->name('login');
Route::post('login', [SessioanControler::class, 'login'])->name('loginn');

// Route::get('home', [SessioanControler::class, 'index'])->name('home')->middleware('auth');
Route::get('logout', [SessioanControler::class, 'logout'])->name('logout');









// Auth::routes(); //bawaan dari otomatis

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
