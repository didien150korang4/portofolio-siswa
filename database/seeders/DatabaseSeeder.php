<?php

namespace Database\Seeders;

use App\Models\Kategori;
use App\Models\Kelas;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;



class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    Kategori::create([
        'kategori' => 'Desain Grafis'
    ]);
    Kategori::create([
        'kategori' => 'Desain Logo'
    ]);
    Kategori::create([
        'kategori' => 'Karya Tulis'
    ]);
    Kelas::create([
        'Kelas' => 'Class X'
    ]);
    Kelas::create([
        'Kelas' => 'Class XI'
    ]);
    Kelas::create([
        'Kelas' => 'Class XII'
    ]);

    User::create([
        'name' => 'Hozairi',
        'role' => 'user',
        'password' => '123456',
        'username' => '202020',
        // 'created_at' => now(),
    ]);
    }
}
